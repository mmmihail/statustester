Rails.application.routes.draw do
  root 'index#index'

  get 'index/index'
  post 'new_site', to: 'index#new_site'
  delete 'destroy_site', to: 'index#destroy_site'
  post 'import_sites', to: 'index#import_sites'

  get 'get_sites', to: 'index#get_sites'

  get 'settings', to: 'index#settings'
  post 'update_settings', to: 'index#update_settings'
  post 'update_email', to: 'index#update_email'

  get 'start_monitoring', to: 'index#start_monitoring'
  get 'stop_monitoring', to: 'index#stop_monitoring'

end

class ChangeSites < ActiveRecord::Migration[5.0]
  def change
    change_table :sites do |t|
      t.change :status, :string,  default: ""
    end
  end
end

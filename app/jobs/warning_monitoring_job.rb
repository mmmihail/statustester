class WarningMonitoringJob < ApplicationJob
  require "redis"
  require 'net/http'

  def perform
    redis = Redis.new
    working = "1"
    redis.set('working',working)
    while working=="1" do
      time = redis.get('time_warning').to_i
      sites = Site.where.not(status: ['ok',''])
      sites.each do |site|
        puts site.url
        str = URI.escape(site.url)
        uri = URI.parse(str)
        begin
          res=Net::HTTP.get_response(uri)
        rescue
          site.status = $!
          site.save
        else
          unless res.code=='200'
            site.status = res.code
            site.save
          else
            site.status = 'ok'
            site.save
            email = redis.get('email')
            if email != ""
              NotificationMailer.ok_email(email, site.id).deliver_now
            end
          end
        end
      end
      sleep time
      working = redis.get('working')
    end
  end

end

class IndexController < ApplicationController
  #http_basic_authenticate_with name: "admin", password: "admin"
  require "redis"
  @@redis = Redis.new
  @@redis.set('working',0)
  @@redis.set('time_normal',20)
  @@redis.set('time_warning',10)
  Resque.workers.each {|w| w.unregister_worker}

  def index
    @sites = Site.all
    @working = @@redis.get('working')
  end

  def settings
    @time_normal = @@redis.get('time_normal').to_i
    @time_warning = @@redis.get('time_warning').to_i
    @email = @@redis.get('email')
    @working = @@redis.get('working')
  end

  def update_settings
    @@redis.set('time_normal',params[:settings][:time_normal])
    @@redis.set('time_warning',params[:settings][:time_normal])
    redirect_to :back
  end

  def update_email
    @@redis.set('email',params[:update_email][:email])
    redirect_to :back
  end

  def new_site
    @site = Site.create(params.require(:site).permit(:url))
    redirect_to :back
  end

  def destroy_site
    id = params[:id]
    site = Site.find(id)
    site.destroy
    redirect_to :back
  end

  def get_sites
    @sites = Site.all
    render json: {sites: @sites, status: @@redis.get('working')}.to_json
  end

  def import_sites
    uploaded = params[:file]
    if uploaded != nil && File.extname(uploaded.original_filename).downcase == '.txt'
      Site.delete_all
      f = uploaded.open
      f.each do |line|
        site = Site.create(url: line.strip)
      end
    end
    redirect_to :back
  end

  def start_monitoring
    NormalMonitoringJob.set(queue: :normal).perform_later
    WarningMonitoringJob.set(queue: :warning).perform_later
    redirect_to :root
  end

  def stop_monitoring
    @@redis.set('working',0)
    Resque.workers.each {|w| w.unregister_worker}
    redirect_to :root
  end
end

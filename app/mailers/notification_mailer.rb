class NotificationMailer < ApplicationMailer

  def warning_email(email, site_id)
    @site = Site.find(site_id)
    mail(to: email, subject: "Проблемы с сайтом #{@site.url}")
  end

  def ok_email(email, site_id)
    @site = Site.find(site_id)
    mail(to: email, subject: "Cайт #{@site.url} снова в норме")
  end
end

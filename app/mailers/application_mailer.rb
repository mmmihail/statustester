class ApplicationMailer < ActionMailer::Base
  default from: 'info@epicdevelopment.ru'
  layout 'mailer'
end

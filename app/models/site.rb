class Site < ApplicationRecord
  validates :url, format: { with: /(https?:\/\/)?([\w\.]+)\.([a-z]{2,6}\.?)(\/[\w\.]*)*\/?/}

  before_create do
    unless (self.url.include? 'http://') || (self.url.include? 'https://')
      self.url = 'http://' + self.url
    end
  end
end
